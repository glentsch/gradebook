

public abstract class GradeBookEntry {

	private Person student;
	private String assessmentName;
	Curve curve;
	
	public GradeBookEntry( String assessmentName ){
		this.assessmentName = assessmentName;
	}

	public GradeBookEntry(Person student, String assessmentName) {
		this.student = student;
		this.assessmentName = assessmentName;
	}


	public GradeBookEntry() {
	}
	
	public void setNumericCurve(int numericCurve){
		curve.setNumericCurve(numericCurve);
	}
	
	public Person getStudent() {
		return student;
	}

	public void setStudent(Person person) {
		this.student = person;
	}

	public String getAssessmentName() {
		return assessmentName;
	}

	public void setAssessmentName(String assessmentName) {
		this.assessmentName = assessmentName;
	}
	


	public void printEntry(){
		System.out.println("Assignment: " + getAssessmentName() );
		System.out.println("For student: " + getStudent().getName() );
		System.out.println("Grade: " + getNumericGrade() );

	}
	public abstract void setNumericGrade(int grade);
	public abstract int getNumericGrade();
	public abstract void getData();
}
