

import java.util.Scanner;

public class ExamEntry extends GradeBookEntry {

	private int numericCurve;
	private int numericGrade;

	public ExamEntry( String examName ){
		super(examName);
	}
	
	public ExamEntry( String examName, int numericCurve ){
		super(examName);
		curve = new curveTrue();
		this.numericCurve = numericCurve;
	}

	public int getNumericCurve() {
		return numericCurve;
	}

	public void setNumericCurve(int curve) {
		this.numericCurve = curve;
	}
	@Override
	public void setNumericGrade(int numericGrade) {
		this.numericGrade = numericGrade;
	}

	@Override
	public int getNumericGrade() {
		return numericGrade + numericCurve;
	}
	
	@Override
	public void getData(){
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Enter the numeric grade: ");
		boolean success = false;
		while (!success) {
			try {
				this.numericGrade = reader.nextInt();
			} catch (Exception e) {
				System.out.println("Invalid, please try again:");
			}
			if( numericGrade >= 0 && numericGrade <= 100 ){
				success = true;
			}else {
				System.out.println("Must be 0-100, Please try again:");
			}
		}
		reader.nextLine();
		
		System.out.println("Enter the curve: ");
		success = false;
		while (!success) {
			try {
				this.numericCurve = reader.nextInt();
			} catch (NumberFormatException e) {
				System.out.println("Invalid, please try again:");
			}
			if( numericCurve >= 0 && numericCurve <= 100 ){
				success = true;
			}else {
				System.out.println("Must be 0-100, Please try again:");
			}
		}
	
	}
	
	@Override
	public String toString(){
		return this.getAssessmentName() + "\n" + this.getStudent().getName() + "\n" +
					this.getNumericGrade() + "\n" + this.getNumericCurve();
					
	}
}
