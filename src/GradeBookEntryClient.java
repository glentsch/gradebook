

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class GradeBookEntryClient {
    Socket socket;
    BufferedReader reader;
    public GradeBookEntryClient(){
    }
    
    public void connect( String hostName, int port ){
    	try{
    	socket = new Socket(hostName, port);
    	}catch(IOException e){
    		System.out.println("Could not connect to server");
    	}
    }
    
    public void setupIoStreams(){
    	try{
    	reader = new BufferedReader(new InputStreamReader(
                socket.getInputStream()));
    	}catch(IOException e){
    		System.out.println("Could not create a stream");
    	}
    }
    
    public void sendMessage(String message){
    	try{
    	PrintWriter writer = new PrintWriter(socket.getOutputStream());
         writer.println(message);
         writer.flush();
    	}catch(IOException e){
    		System.out.println("Could not send a message to the server");
    	}
    }
    
    public void listenForMessage(){
    	String serverMessage = null;
    	try{
    	serverMessage = reader.readLine();
    	while(!serverMessage.startsWith("EOL:")){
    	System.out.println(serverMessage);
    	serverMessage = reader.readLine();
    	}
    	}catch(IOException e){
    		System.out.println("Could not read a message");
    	}
    }
    
    public void showMenu(){
    	Scanner scanner = new Scanner(System.in);
    	
    	boolean keepGoing = true;
    	
    	while(keepGoing){
    		System.out.println("0: exit");
    		System.out.println("1: Submit a grade");
    		System.out.println("2: See all gradebook entries");
    		System.out.println("3: View all Students");

    		int choice = 0;
    		try{
    		choice = scanner.nextInt();
    		}catch(Exception e){
    			System.out.println("Error " + e);
    		}
    		switch(choice){
    		case 0:
    			sendMessage("Stop");
    			keepGoing = false;
    			scanner.close();
    			break;
    		case 1:
    			System.out.println("Enter student name");
    			scanner.nextLine();
    			String name= scanner.next();
    			GradeBookEntry book = GradeBook.createEntry(name);
    			sendGradeBookEntry(book);
    			break;
    		case 2:
    			sendMessage("ListEntries:");
    			setupIoStreams();
    			listenForMessage();
    			break;
    		case 3:
    			sendMessage("GetStudents:");
    			setupIoStreams();
    			listenForMessage();
    			break;
    		default: 
    			break;
    		}

    		}
    	
    	}
    		
    		
    
    
	private void sendGradeBookEntry(GradeBookEntry entry) {

		String entryData = entry.toString().trim().replaceAll("\n", ":");
		

		// To read the correct Entry back it will be helpful to indicate
		// which kind it is in the file record.
		if (entry instanceof ExamEntry) {
			entryData = "exam:" + entryData;
		} else {
			entryData = "hw:" + entryData;
		}
		System.out.println(entryData);
		sendMessage(entryData);
	}

	public void closeConnection(){
    }

	public static void main(String[] args) {
		GradeBookEntryClient client = new GradeBookEntryClient();
		
		client.connect("localhost", 5678);
		client.showMenu();
	}

}
