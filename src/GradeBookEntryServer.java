

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;


public class GradeBookEntryServer {
	
	private ServerSocket serverListenerSocket;
	private Socket gradeBookSocket;
    private BufferedReader socketReader;
    private PrintWriter socketWriter;
    private static GradeBook gradeBook;

	public GradeBookEntryServer() {
		this.serverListenerSocket = null;
		this.gradeBookSocket = null;
		this.socketReader = null;
		this.socketWriter = null;
	}
	
	public void startServer( int port ){
		try {
			serverListenerSocket = new ServerSocket(port);
		} catch (IOException e) {
			System.out.println("Error stating server on port " + port);
		}
	}
	public Person findStudent(String name){
		for(Person student : gradeBook.getStudents()){
			if(student.getName().equals(name)){
				return student;
			}
		}
		Person student = new Person(name);
		gradeBook.addStudent(student);
		return student;
	}
	
	public void waitForConnection(){

		try {
			this.gradeBookSocket = serverListenerSocket.accept();
			 this.socketWriter = new PrintWriter(gradeBookSocket.getOutputStream() );
			 this.socketReader = new BufferedReader(new InputStreamReader(
					 gradeBookSocket.getInputStream()));
		} catch (IOException e) {
			System.out.println("Error getting connection: " + e.getMessage());
		}
	}
	
	public void sendMessage(String message){
    	System.out.println("debug: sending message to client: " + message);
    	socketWriter.println(message);
    	socketWriter.flush();
    }
    
    public String listenForMessage(){
    	String serverMessage = null;
    	try {
			serverMessage = socketReader.readLine();
		} catch (IOException e) {
			System.out.println("Couldn't get message from the client!" );
		}
    	
    	System.out.println("Return message["+serverMessage+"]" );
    	return serverMessage;
    }
    
    public void closeConnection(){
    	try {
			gradeBookSocket.close();
			socketReader.close();
	    	socketWriter.close();
		} catch (IOException e) {
			System.out.println("Error shutting down connection to client.");
		}
    }

	public static void main(String[] args) {
		System.out.println("Starting the server");
		gradeBook = new GradeBook("Test Course");
		Person person1 = new Person("John");
		Person person2 = new Person("Lisa");
		gradeBook.addStudent(person1);
		gradeBook.addStudent(person2);
		ArrayList<GradeBook> books = new ArrayList<GradeBook>();
		GradeBookEntryServer server = new GradeBookEntryServer();
		books.add(gradeBook);
		server.startServer(5678);
		server.waitForConnection();

		String clientMsg;
		boolean run = true;
		System.out.println("Got connection, waiting for message...");
		while (run) {
			clientMsg = server.listenForMessage();

			System.out.println("Client Message: " + clientMsg);
			if (clientMsg.startsWith("GetStudents:")) {
				StringBuilder sb = new StringBuilder();
				for (Person student : gradeBook.getStudents()) {
					sb.append(student.getName() + ":");
				}
				System.out.println("sending students:\n" + sb.toString());
				server.sendMessage(sb.toString());
				server.sendMessage("EOL:");
			}
			else if (clientMsg.startsWith("exam:") || clientMsg.startsWith("hw:")) {
				String[] fields = clientMsg.split(":");
					GradeBookEntry entry = null;
				if (fields[0].equals("exam")) {
					entry = new ExamEntry(fields[1]);
					entry.setStudent(server.findStudent(fields[2]));
					// The entry gets written with the curve added in
					// So subtract the curve to get their base grade.
					int numericGradeWithCruve = Integer.parseInt(fields[3]);
					int curve = Integer.parseInt(fields[4]);
					entry.setNumericGrade(numericGradeWithCruve - curve);
					entry.setNumericCurve(curve);
					System.out.println("Adding entry:\n" + entry.toString());
					gradeBook.putEntry(entry);
				} else {
					entry = new HomeworkEntry(fields[1]);
					entry.setStudent(server.findStudent(fields[2]));
					entry.setNumericGrade(Integer.parseInt(fields[3]));
					System.out.println("Adding entry:\n" + entry.toString());
					gradeBook.putEntry(entry);
				}
			} 
			else if (clientMsg.startsWith("ListEntries:")) {
				for( GradeBookEntry entry: gradeBook.getEntries() ){
					String message = null;
					if( entry instanceof ExamEntry ){
						message = "exam:" + entry.toString().trim().replaceAll("\n", ":");
					}else {
						message = "hw:" + entry.toString().trim().replaceAll("\n", ":");
					}
					server.sendMessage(message);
				}
				// send End Of List
				server.sendMessage("EOL:");
			}
			else if (clientMsg.startsWith("Stop")) {
				run = false;
			} 
			else {
				System.out.println("Invalid client message["+clientMsg + "]");
				server.sendMessage("SERVER SAYS: Invalid command from client.");
			}
		}

		server.closeConnection();
		System.out.println("I closed everything, now I am done!");
	}

}

