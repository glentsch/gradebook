

import java.util.ArrayList;
import java.util.Scanner;

public class GradeBook {
	
	private String course;
	private ArrayList<Person> students = new ArrayList<Person>();
	private ArrayList<GradeBookEntry> entries = new ArrayList<GradeBookEntry>();
	
	public GradeBook(String course){
		this.course = course;
	}
	
	public String getCourse() {
		return course;
	}

	public ArrayList<Person> getStudents() {
		return students;
	}

	public ArrayList<GradeBookEntry> getEntries() {
		return entries;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public void addStudent( Person student ){
		students.add(student);
	}
	public String findStudent(){
		System.out.println("Grade which student: ");
		for (int i = 0; i < students.size(); i++) {
			System.out.println(i + " " + students.get(i).getName());
		}
		Scanner reader = new Scanner(System.in);
		int studentIndex = reader.nextInt();
		reader.close();
		return students.get(studentIndex).getName();
	}
	public void addEntry(){
		System.out.println("Grade which student: ");
		for (int i = 0; i < students.size(); i++) {
			System.out.println(i + " " + students.get(i).getName());
		}
		Scanner reader = new Scanner(System.in);
		int studentIndex = reader.nextInt();
		reader.nextLine();
		
		System.out.println("Enter the assessment name: ");
		String assessmentName = reader.nextLine();
		
		System.out.println("Homework (1) or exam (2): ");
		int entryType = reader.nextInt();
		reader.nextLine();
		
		GradeBookEntry newEntry;
		if( entryType == 1 ){
			newEntry = new HomeworkEntry(assessmentName);
		}else{
			newEntry = new ExamEntry(assessmentName);
		}
		
		newEntry.getData();
		newEntry.setStudent(students.get(studentIndex));
		
		entries.add(newEntry);
		reader.close();
	}
	
	public static GradeBookEntry createEntry(String studentName){
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Enter the assessment name: ");
		String assessmentName = reader.nextLine();
		
		System.out.println("Homework (1) or exam (2): ");
		int entryType = reader.nextInt();
		reader.nextLine();
		
		GradeBookEntry newEntry;
		if( entryType == 1 ){
			newEntry = new HomeworkEntry(assessmentName);
		}else{
			newEntry = new ExamEntry(assessmentName);
		}
		newEntry.setStudent(new Person(studentName));
		newEntry.getData();
		return newEntry;
	}
	
	public void putEntry( GradeBookEntry entry ){
		entries.add(entry);
	}
	
	public void listGrades(){
		for( int i=0; i<entries.size(); i++ ){
			GradeBookEntry entry = entries.get(i);
			entry.printEntry();
			// This could also be condensed to one line:
			// entries.get(i).printEntry();
		}
	}
	
	
}
