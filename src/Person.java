

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Person {

	private String name;
	private int age;
	private String email;
	private String ssn;
	
	public Person(){
		name = null;
		age = 0;
		email = null;
		ssn = null;
	}
	
	public Person(String name){
		// Keyword this distinguishes between the member of this class,
		// and the parameter name;
		this.name = name;
		
		// the rest of the member variables will have default values
	}
	
	// This constructor initializes everything
	public Person( String name, int age, String email, String ssn){
		this.name = name;
		this.age = age;
		this.email = email;
		this.ssn = ssn;
	}
	
	// This is a copy constructor.  It takes an object of the same
	// class, and copies all the member variables over.
	// Then you will have two separate objects with identical data
	public Person( Person person){
		this.name = person.name;
		this.age = person.age;
		this.email = person.email;
		this.ssn = person.ssn;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}
	
	public boolean isEmailValid(String email){
		return email.matches("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$");
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public void getData(){
		Scanner reader = new Scanner(System.in);
		
		System.out.print("Enter the person's name: ");
		name = reader.nextLine();
		
		System.out.print("Enter the person's age: ");
		age = reader.nextInt();
		reader.nextLine();
		
		System.out.print("Enter the person's email: ");
		email = reader.nextLine();
		if( !isEmailValid(email)){
			System.out.println("Hey that's not a valid address!");
		}
		
		System.out.println("Enter SSN: ");
		ssn = reader.nextLine();
		reader.close();
	}
	
	/**
	 * New method for Lab.  Uses a scanner to read the file data.
	 * It is assumed that only data for one person is in the file.
	 * @param fileName The name of the file with person data.
	 */
	public void getDataFromFile(String fileName){
		Scanner reader = null;
		try {
			reader = new Scanner(new File(fileName));
		} catch (FileNotFoundException e) {
			System.out.println("Error opening file");
		}
		
		if (null != reader) {
			name = reader.nextLine();
			age = reader.nextInt();
			reader.nextLine();
			email = reader.nextLine();
			ssn = reader.nextLine();
		}
	}
	
	/**
	 * New method for lab.  This method writes the person data to a file.
	 * @param fileName Name of the file write the data.
	 */
	public void saveToFile(String fileName){
		BufferedWriter writer = null;
		try {
			// This will overwrite any previous file with this name
			writer = new BufferedWriter(new FileWriter(new File(fileName)));
		} catch (IOException e) {
			System.out.println("Undable to open file for writing!");
		}
		
		try {
			// The write method simply takes a string and writes it to the file.
			writer.write(name + "\n");
			writer.write(age + "\n");
			writer.write(email + "\n");
			writer.write(ssn + "\n");
			writer.close();
		} catch (IOException e) {
			System.out.println("Error writing to file!");
		}
	}
	
	
	public String getId(){
		return ssn;
	}
	
	public String toString(){
		return "Name: " + name + "\nAge: " + age + "\nemail: " + email +
				"\nssn: " + ssn;
	}
	
	public static void main( String args[] ){
		Person person = new Person("John Smith", 34, "john@smith.com", "111-22-3333");
		person.saveToFile("johnSmith.txt");
		
		Person john = new Person();
		john.getDataFromFile("johnSmith.txt");
		
		System.out.println("Read from file:\n" + john.toString());
	}
}
