

import java.util.Scanner;

public class HomeworkEntry extends GradeBookEntry {

	private int numericGrade;
	
	public HomeworkEntry( String assignmentName){
		super(assignmentName);
		curve = new curveFalse();
	}
	
	@Override
	public int getNumericGrade() {
		return numericGrade;
	}
	@Override
	public void setNumericGrade(int numericGrade) {
		this.numericGrade = numericGrade;
	}

	@Override
	public void getData(){
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Enter the numeric grade: ");
		boolean success = false;
		while (!success) {
			try {
				this.numericGrade = reader.nextInt();
			} catch (Exception e) {
				System.out.println("Invalid, please try again:");
			}
			if( numericGrade >= 0 && numericGrade <= 100 ){
				success = true;
			}else {
				System.out.println("Must be 0-100, Please try again:");
			}
		}
		reader.nextLine();
	}
	
	@Override
	public String toString(){
		return this.getAssessmentName() + "\n" + this.getStudent().getName() + "\n" +
					this.getNumericGrade();
	}
}
